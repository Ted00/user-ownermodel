from django.urls import path

from property.views import OwnerCreateView

urlpatterns = [
    path('signup/', OwnerCreateView.as_view(), name='signup'),
]