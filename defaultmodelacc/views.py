from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views import generic


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    # /home_rent/venv/lib64/python3.7/site-packages/django/contrib/auth/forms.py
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
    # fields = ['username', 'password1', 'password2', 'first_name', 'last_name', 'email']
    # def form_valid(self, form):
    #     """If the form is valid, save the associated model."""
    #     self.object = form.save(commit=False)
    #     self.object.author = self.request.user
    #     self.object.save()
    #     return super().form_valid(form)
    #
    # def form_invalid(self, form):
    #     pass