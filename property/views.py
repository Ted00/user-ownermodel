from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django_filters.views import BaseFilterView

from property.filters import HomeFilter
from property.forms import HomeForm, OwnerForm
from property.models import Home, Owner


class HomeListView(ListView, BaseFilterView):
    model = Home
    template_name = 'home.html'
    filterset_class = HomeFilter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['home'] = Home.objects.filter()
        context['filter'] = HomeFilter
        return context

    @staticmethod
    def product_list(request):
        f = HomeFilter(request.GET, queryset=Home.objects.all())
        return render(request, 'home.html', {'filter': f})


class OwnerListView(ListView):
    model = Owner
    template_name = 'owners.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['home'] = Home.objects.filter()
        context['owner'] = Owner.objects.filter()
        return context


class HomeDetailView(DetailView):
    model = Home
    template_name = 'home_details.html'


class HomeUpdateView(UpdateView):
    model = Home
    template_name = 'home_update.html'
    fields = ['area', 'address', 'city', 'state', 'rent']
    context_object_name = 'updatehome'


class HomeCreateView(CreateView):
    model = Home
    template_name = 'home_update.html'
    form_class = HomeForm
    context_object_name = 'createhome'

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        self.object.owner = self.request.user.owner
        self.object.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        pass


class HomeDeleteView(DeleteView):
    template_name = 'home_delete.html'
    model = Home
    context_object_name = 'deletepost'
    success_url = reverse_lazy('home')


def product_list(request):
    f = HomeFilter(request.GET, queryset=Home.objects.all())
    return render(request, 'home.html', {'filter': f})


class OwnerCreateView(CreateView):
    model = Owner
    form_class = OwnerForm
    template_name = 'signup.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        user = User(
            username=form.cleaned_data['username'],
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name'],
            email=form.cleaned_data['email'],
        )
        user.set_password(form.cleaned_data['password1'])
        user.save()
        self.object.user = user
        self.object.save()
        return super().form_valid(form)
        # self.object = form.save(commit=False)
        # self.object.first_name = User.username
        # self.object.last_name = User.last_name
        # self.object.email = User.email
        # self.object.save()
        # return super().form_valid(form)

    def form_invalid(self, form):
        pass
