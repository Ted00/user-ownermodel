from django import forms
from django.forms import ModelForm
from django.template import loader, RequestContext

from property.models import Owner, Home


class HomeForm(ModelForm):
    # fields = ['area', 'address', 'city', 'state', 'rent', 'owner']
    class Meta:
        model = Home
        # fields = '__all__'
        fields = ['area', 'address', 'city', 'state', 'rent']
    # area = forms.IntegerField(min_value=0)
    # address = forms.CharField(max_length=100)
    # city = forms.CharField(max_length=20)
    # state = forms.CharField(max_length=20)
    # rent = forms.IntegerField(min_value=0)
    # owner = forms.Select(choices=Owner.objects.filter())


class OwnerForm(ModelForm):
    class Meta:
        model = Owner
        fields = ['username', 'first_name', 'last_name', 'phone', 'email', 'password1']
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=20)
    last_name = forms.CharField(max_length=20)
    email = forms.CharField(max_length=40)
    password1 = forms.CharField(max_length=20)
