from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django_filters.views import FilterView

from property import views
from property.models import Home
from property.views import HomeCreateView

urlpatterns = [
    path('', views.product_list, name='home'),
    path('home/<int:pk>/', views.HomeDetailView.as_view(), name='home_detail'),
    path('update/<int:pk>/', views.HomeUpdateView.as_view(), name='home_update'),
    path('create/', HomeCreateView.as_view(), name='home_create'),
    path('delete/<int:pk>/', views.HomeDeleteView.as_view(), name='home_delete'),
    path('owners/', views.OwnerListView.as_view(), name='owners'),
    path('newowner/', views.OwnerCreateView.as_view(), name='owner_create'),
    # url(r'^list/$', FilterView.as_view(model=Home)),
    # path('filter/', views.product_list, name='filters'),
]
