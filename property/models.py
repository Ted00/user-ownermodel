from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse

from defaultmodelacc.views import SignUpView


class Home(models.Model):
    area = models.PositiveIntegerField(blank=True, null=False)
    address = models.CharField(max_length=300)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50, default='Albania')
    rent = models.PositiveIntegerField(blank=False)
    owner = models.ForeignKey(
        'Owner',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.city + ' ' + self.state

    def get_absolute_url(self):
        return reverse('home')


class Owner(models.Model):
    user = models.OneToOneField(  # change to models.OneToOneField
        'auth.User',
        on_delete=models.CASCADE, related_name='owner'
    )
    phone = models.IntegerField(verbose_name='Phone number')

    def __str__(self):
        return self.user.username
